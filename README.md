# Skeleton project for Swagger

Swagger Editor
Swagger Editor lets you edit API specifications in YAML inside your browser and to preview documentations in real time. Valid Swagger JSON descriptions can then be generated and used with the full Swagger tooling (code generation, documentation, etc).

To understand how it works, you should try the live demo! http://editor.swagger.io/#/

**Steps**

* **git clone https://js4lazar@bitbucket.org/js4lazar/swagger-ui-editor.git**
* **cd folder swagger-editor**
* **NPM install**
* **npm install -g swagger**
* **swagger project edit swagger-editor**

Example Skeleton services API Tables 

El archivo generado con la API , se puede apreciar en el directorio  **api/swagger/**swagger.yaml